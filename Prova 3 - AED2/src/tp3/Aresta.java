/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

/**
 *
 * @author Rodrigo
 */
public class Aresta {
    
    public int vertice_a;
    public int vertice_b;
    public float peso;
    
    public Aresta(int vertice_a,int vertice_b,int peso){
        this.vertice_a = vertice_a;
        this.vertice_b = vertice_b;
        this.peso = peso;
    }
    
    @Override
    public boolean equals(Object o){ // considerados iguais: (a->b) e (b<-a)
        if(!(o instanceof Aresta)) return false;
        
        Aresta aux = (Aresta)o;
        if(((aux.vertice_a == vertice_a && aux.vertice_b == vertice_b)||(aux.vertice_a == vertice_b && aux.vertice_b == vertice_a)) && aux.peso == peso)
            return true;
        return false;
    }
}
