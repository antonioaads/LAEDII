/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import java.util.ArrayList;

/**
 *
 * @author aluno
 */
public class TP3 {
    int matriz[][];
    /**
     * @param args the command line arguments
     */
    
    private static void imprimirResultado(String nome, ArrayList<Integer> arr){
        if(arr!=null && arr.size()>0){
            System.out.println("--------- "+nome+" - CAMINHO ESCOLHIDO --------- ");
            System.out.println("     _\n" +
                                "   _| |\n" +
                                " _| | |\n" +
                                "| | | |\n" +
                                "| | | | __\n" +
                                "| | | |/  \\\n" +
                                "|       /\\ \\\n" +
                                "|      /  \\/\tdeu good\n" +
                                "|      \\  /\\\n" +
                                "|       \\/ /\n" +
                                " \\        /\n" +
                                "  |     /\n" +
                                "  |    |");
            System.out.println("Distancia total:"+Integer.toString(arr.get(0)));
            System.out.println("Vertices:");
            for(int i=1;i<arr.size();i++){
                System.out.print(Integer.toString(arr.get(i))+" ");
            }
            System.out.println();
        }else{
            System.out.println("--------- "+nome+" - CAMINHO NÃO ENCONTRADO --------- ");
            System.out.println("não fique triste, olha só esse gatinho :3\n"+"    /\\_____/\\\n" +
                                                                            "   /  o   o  \\\n" +
                                                                            "  ( ==  ^  == )\n" +
                                                                            "   )         (\n" +
                                                                            "  (           )\n" +
                                                                            " ( (  )   (  ) )\n" +
                                                                            "(__(__)___(__)__)");
        }
    }
    
    public static void main(String[] args) {
        
        for(int i=3;i<=9;i++){
            int tamanho = i;
            MatrizAdjacencia matriz = new MatrizAdjacencia(tamanho);
            matriz.preencher();
            matriz.imprimir();

            FB forca_bruta_aleatorio = new FB(matriz);
            HR heuristica_aleatorio = new HR(matriz);

            long t0 = System.currentTimeMillis();
            ArrayList<Integer> melhor_caminhoFB = forca_bruta_aleatorio.getCaixeiro(0);
            long t1 = System.currentTimeMillis();
            ArrayList<Integer> melhor_caminhoHR = heuristica_aleatorio.getCaixeiro(0);
            long t2 = System.currentTimeMillis();

            imprimirResultado("Força bruta/Grafo Aleatório ("+Integer.toString(i)+")",melhor_caminhoFB);
            System.out.println("Tempo gasto: "+Long.toString(t1-t0));
            imprimirResultado("Heurística/Grafo Aleatório ("+Integer.toString(i)+")", melhor_caminhoHR);
            System.out.println("Tempo gasto: "+Long.toString(t2-t1));
        }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        
        
        MatrizAdjacencia m1 = new MatrizAdjacencia("matrizes/si535_UP.txt",535,MatrizAdjacencia.DIAGONAL_SUPERIOR);
        HR heuristica_m1 = new HR(m1);
        ArrayList<Integer> melhor_caminhoM1 = heuristica_m1.getCaixeiro(0);
        imprimirResultado("Heurística/si535", melhor_caminhoM1);
   
        
        MatrizAdjacencia m2 = new MatrizAdjacencia("matrizes/pa561_LOW.txt",561,MatrizAdjacencia.DIAGONAL_INFERIOR);
        HR heuristica_m2 = new HR(m2);
        ArrayList<Integer> melhor_caminhoM2 = heuristica_m2.getCaixeiro(0);
        imprimirResultado("Heurística/sipa561", melhor_caminhoM2);
        
        
        MatrizAdjacencia m3 = new MatrizAdjacencia("matrizes/si1032_UP.txt",1032,MatrizAdjacencia.DIAGONAL_SUPERIOR);
        HR heuristica_m3 = new HR(m3);
        ArrayList<Integer> melhor_caminhoM3 = heuristica_m3.getCaixeiro(0);
        imprimirResultado("Heurística/si1032", melhor_caminhoM3);
    }
}
