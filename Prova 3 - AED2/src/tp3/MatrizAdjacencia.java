/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author aluno
 */
public class MatrizAdjacencia {
    public static int DIAGONAL_SUPERIOR = 0;
    public static int DIAGONAL_INFERIOR = 1;
    
    
    public int matriz[][];
    public int tamanho;
    
    public MatrizAdjacencia(){
        
    }
    
    public MatrizAdjacencia(int tamanho){
        this.tamanho = tamanho;
        matriz = new int[tamanho][tamanho];
    }
    
    public MatrizAdjacencia(String path,int tamanho,int TIPO){
        this.tamanho = tamanho;
        
        ArrayList<Integer> arr = new ArrayList<>();
        
        try {
            File f = new File(path);
            Scanner input = new Scanner(f);

            while(input.hasNext()){
                int valor = input.nextInt();
                if(TIPO==DIAGONAL_INFERIOR && valor==0 && input.hasNext())
                    valor = input.nextInt();
                arr.add(valor); // o arquivo de texto diag.Inf. possui zeros da diag. principal
            }
            
            input.close();
//            System.out.println("Total de valores "+Integer.toString(arr.size()));
        } catch (Exception e) {
            System.out.println("Erro na leitura do arquivo: "+e.toString());
        }
        
        matriz = new int[tamanho][tamanho];
        zerar();

        if(TIPO == DIAGONAL_SUPERIOR){
            // preencher diagonal superior
            for(int i=0,n=0; i<tamanho-1;i++){
                for(int j=i+1; j<tamanho;j++){
                    matriz[i][j]=arr.get(n);
                    matriz[j][i]=arr.get(n++);
                }
            }
        }
        else if(TIPO == DIAGONAL_INFERIOR){
            // preencher diagonal inferior
            for(int i=1,n=0; i<tamanho;i++){
                for(int j=0; j<i;j++){
                    matriz[i][j]=arr.get(n);
                    matriz[j][i]=arr.get(n++);
                }
            }
        }
    }
    
    public void zerar(){
        for(int i = 0; i<tamanho;i++){
            for(int j = 0; j<tamanho; j++){
                matriz[i][j]=-1; // não existe caminho
            }
        }
    }
    
    public void preencher(){
        zerar();
        
        Random gerador = new Random();
        int aleatorio;
        for(int i = 0; i<tamanho;i++){
            for(int j = i; j<tamanho; j++){
                if(i!=j){
                    aleatorio = gerador.nextInt(100);
                    matriz[i][j]=aleatorio; //gera números randômicos de 0 a 100.
                    matriz[j][i]=aleatorio; //gera números randômicos de 0 a 100.
                }
            }
        }
    }
    
    public void imprimir(){
        for(int i = 0; i<tamanho;i++){
            for(int j = 0; j<tamanho; j++){
                System.out.print(" " + matriz[i][j] + " ");
            }
            System.out.println();
        }
    }

    public int[][] getMatriz() {
        return matriz;
    }
    
    public ArrayList<Integer> getVerticesAdjacentes(int v){
        ArrayList<Integer> arr = new ArrayList<Integer>();
        for(int j=0;j<tamanho;j++){
            if(matriz[v][j]!=-1 && v!=j){
                arr.add(j);
            }
        }
        return arr;
    }
    
    public int getPeso(int A, int B){
        return matriz[A][B];
    }
}
