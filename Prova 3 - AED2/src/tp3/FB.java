/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp3;

import java.util.ArrayList;

/**
 *
 * @author aluno
 */
public class FB {
    MatrizAdjacencia matriz;
    
    int vertice_inicial;
    ArrayList<Integer> caminho_recursao;
    
    ArrayList<ArrayList<Integer>> caminhos_validos;
    ArrayList<Integer> menor_caminho;
    
    public FB(int tamanho){
        this.matriz  = new MatrizAdjacencia(tamanho); // Gerado aleatoriamente
        vertice_inicial = -1;
    }
    
    public FB(MatrizAdjacencia mx){
        this.matriz  = mx; // Gerado aleatoriamente
        vertice_inicial = -1;
    }

    private boolean verticeInicialProximo(int v){
        for(int vx: matriz.getVerticesAdjacentes(v))
            if(vx == vertice_inicial)
                return true;
        return false;
    }
    
    public boolean tudoVisitado(){
        if(caminho_recursao.size()-1 == matriz.tamanho) // -1 devido ao primeiro elemento ser o peso total do caminho
            return true;
        return false;
    }

    public ArrayList<Integer> getCaixeiro(int v){
        vertice_inicial = v;
        menor_caminho = new ArrayList<Integer>();
        caminhos_validos = new ArrayList<ArrayList<Integer>>();
 
        // Criar caminhos
        caminho_recursao = new ArrayList<Integer>();
        caminho_recursao.add(0); // peso do caminho sendo feito
        caminho_recursao.add(v);
        
        proxVerticeRecursivoMemOptmized(v);

        return menor_caminho;
    }
    
    public void proxVerticeRecursivoMemOptmized(int v){
        // Pegar vértices vizinhos
        ArrayList<Integer> vizinhos = matriz.getVerticesAdjacentes(v);

//        System.out.println("Caminho realizado até então:");
//        for(int vz: caminho_recursao)
//            System.out.print(Integer.toString(vz)+" ");
//        System.out.println();
//        
//        System.out.println(Integer.toString(vizinhos.size())+" vizinhos de "+Integer.toString(v));
//        for(int vz: vizinhos)
//            System.out.println(vz);
        
        // remover vizinhos que já fazem parte do caminho
        for(int i=0;i<vizinhos.size();){
            
            boolean existe = false;
            for(int j=1;j<caminho_recursao.size();j++)
                if((int)vizinhos.get(i)==(int)caminho_recursao.get(j))
                    existe = true;
            
            if(existe)
                vizinhos.remove(i);
            else
                i++;
        }

//        System.out.println(Integer.toString(vizinhos.size())+" vizinhos nao visitados de "+Integer.toString(v));
//        for(int vz: vizinhos)
//            System.out.println(vz);
        
        while(!vizinhos.isEmpty()){
            // Pega próximo vértice e coloca na lista do caminho feito até então
            int proxV = vizinhos.get(0);
            vizinhos.remove(0);
            caminho_recursao.add(proxV);
            
//            System.out.println("Vizinho escolhido:"+Integer.toString(proxV));
            
            // Colocar peso da aresta escolhida no somatório total
            int peso_aresta = matriz.getPeso(v, proxV);
            caminho_recursao.set(0,caminho_recursao.get(0)+peso_aresta);
            
            proxVerticeRecursivoMemOptmized(proxV);
            
            // remove peso e vertice devido a backtracking
            caminho_recursao.remove(caminho_recursao.size()-1);
            caminho_recursao.set(0,caminho_recursao.get(0)-peso_aresta);
        }
        
        // condição de validação -> encontrar o vértice inicial novamente (ele é adjacente) e ter visitado todos os vértices
        if(tudoVisitado() && verticeInicialProximo(v)){
            // adiciona última aresta
            caminho_recursao.add(vertice_inicial);
            int peso_aresta = matriz.getPeso(v, vertice_inicial);
            caminho_recursao.set(0,caminho_recursao.get(0)+peso_aresta);
            
            if(menor_caminho.size()==0)
                menor_caminho = (ArrayList<Integer>)caminho_recursao.clone();
            else if(caminho_recursao.get(0)<menor_caminho.get(0))
                menor_caminho = (ArrayList<Integer>)caminho_recursao.clone();
//            System.out.println("NOVO CAMINHO VALIDADO | "+Integer.toString(caminhos_validos.size())+" caminho(s) valido(s)");
        
            // remove peso e vertice devido a backtracking
            caminho_recursao.remove(caminho_recursao.size()-1);
            caminho_recursao.set(0,caminho_recursao.get(0)-peso_aresta);
        }
        
        return; // Fim do caminho
    }
}
